from datetime import datetime
import json
import time
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys

# import datetime


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


def update_account_vo(ch, method, properties, body):
    # Declare a function to update the AccountVO object
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)
    if is_active:
        # Use the update_or_create method of the AccountVO.objects QuerySet
        # to update or create the AccountVO object
        AccountVO.objects.update_or_create(
            first_name=first_name,
            last_name=last_name,
            email=email,
            is_active=is_active,
            updated=updated,
        )

    else:

        AccountVO.objects.delete(email=email)

    # Based on the reference code at
    #   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
    # infinite loop


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.exchange_declare(
            exchange="account_info", exchange_type="fanout"
        )
        result = channel.queue_declare(queue="", exclusive=True)
        queue_name = result.method.queue
        channel.queue_declare(queue="queue_name")
        channel.queue_bind(exchange="account_info", queue="queue_name")
        channel.basic_consume(
            queue="queue_name",
            on_message_callback=update_account_vo,
            auto_ack=True,
        )

        channel.start_consuming()

    except AMQPConnectionError:
        #       print that it could not connect to RabbitMQ
        print("Could not connect to RabbitMQ")
        #       have it sleep for a couple of seconds
        time.sleep(2.0)
